---
active: true
date: 2017-11-04T12:54:31+01:00
weight: 2
color: blue
width: 1
---

# Hack2Eaux
*Hack2Eaux* est une initiative collective, poussée par *Kaouenn-noz*, dans le but de comprendre les enjeux relatifs à l'eau. 

Plus d'informations sur le [wiki](https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux).
